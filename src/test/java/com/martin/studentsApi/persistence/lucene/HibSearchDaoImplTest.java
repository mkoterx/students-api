package com.martin.studentsApi.persistence.lucene;

import com.martin.studentsApi.model.Student;
import com.martin.studentsApi.model.StudyProgram;
import com.martin.studentsApi.persistence.HibSearchDao;
import com.martin.studentsApi.persistence.StudentDAO;
import com.martin.studentsApi.persistence.StudyProgramDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest //  focuses only on JPA components
@ActiveProfiles({"test"})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class HibSearchDaoImplTest {

    @Autowired
    private HibSearchDao hibSearchDao;

    @Autowired
    private StudentDAO studentDAO;

    @Autowired
    private StudyProgramDAO studyProgramDAO;

    // Is there a need for an entity manager?
    @PersistenceContext
    private EntityManager entityManager;

    @Before
    public void setUp() {
        // Save study programs
        StudyProgram kni = new StudyProgram( 1L, "KNI");
        System.out.println(this.studyProgramDAO);
        this.studyProgramDAO.save(kni);
        StudyProgram pet = new StudyProgram( 2L, "PET");
        this.studyProgramDAO.save(pet);

        // Save students
        Student martin = new Student( 1L, "Martin", "Kotevski", kni);
        this.studentDAO.save(martin);
        Student alek = new Student( 2L, "Alek", "Ivanovski", kni);
        this.studentDAO.save(alek);
        Student filip = new Student( 3L, "Filip", "SImonovski", pet);
        this.studentDAO.save(filip);
        Student kosta = new Student( 4L, "Kostadin", "Martin", kni);
        this.studentDAO.save(kosta);
    }

    @Test
    public void testSearchByKeyword() {
        List<Student> actual =
                hibSearchDao.searchByKeyword(Student.class, "martin", "firstName", "lastName");
        System.out.println("Actual" + actual);
    }

}