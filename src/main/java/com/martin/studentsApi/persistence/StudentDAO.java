package com.martin.studentsApi.persistence;

import com.martin.studentsApi.model.Student;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface StudentDAO extends CrudRepository<Student, Long>, JpaSpecificationExecutor {

    Optional<Student> findById(Long id);

    List<Student> findByStudyProgramId(Long id);
}
