package com.martin.studentsApi.persistence.lucene;

import com.martin.studentsApi.persistence.HibSearchDao;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HibSearchDaoImpl implements HibSearchDao {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * A basic search for the a given entity. The search is done by exact match per
     * keywords on the specified fields.
     *
     * @param text The query text.
     */
    public <T> List<T> searchByKeyword(Class<T> entityClass, String text, String... fields) {
        // TODO implement this method not to use exact matching
        // get the full text entity manager
        FullTextEntityManager fullTextEntityManager =
                Search.getFullTextEntityManager(entityManager);

        // create the query using Hibernate Search query DSL
        QueryBuilder queryBuilder =
                fullTextEntityManager.getSearchFactory()
                        .buildQueryBuilder()
                        .forEntity(entityClass).get();

        // a very basic query by keywords
        org.apache.lucene.search.Query query =
                queryBuilder
                        .keyword()
                        .onFields(fields)
                        .matching(text)
                        .createQuery();

        // wrap Lucene query in an Hibernate Query object
        org.hibernate.search.jpa.FullTextQuery jpaQuery =
                fullTextEntityManager.createFullTextQuery(query, entityClass);

        // execute search and return results (sorted by relevance as default)
        @SuppressWarnings("unchecked")
        List<T> results = jpaQuery.getResultList();

        return results;
    }

}
