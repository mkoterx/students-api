package com.martin.studentsApi.persistence;

import com.martin.studentsApi.model.Student;
import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * To use this class with @DataJpaTest annotation while testing, it must implement Repository.
 * Repository<Student, Long> is randomly chosen, any other class marked for ORM can be used.
 */
public interface HibSearchDao extends Repository<Student, Long> {
    <T> List<T> searchByKeyword(Class<T> entityClass, String text, String... fields);
}