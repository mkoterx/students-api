package com.martin.studentsApi.model;

//import org.hibernate.annotations.CacheConcurrencyStrategy;
//import org.hibernate.annotations.Cache;
//import org.hibernate.search.annotations.*;

import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.util.List;


@Indexed
@NamedEntityGraph(
        name = "graph.StudyProgram.students",
        attributeNodes = {
                @NamedAttributeNode("students")
        })
@Entity
@Table(name = "study_programs")
/*
@Cacheable //  good convention, although not required by Hibernate
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "studyProgram") */
public class StudyProgram {

    public StudyProgram() {} // JPA only

    public StudyProgram(String name) {
        this.name = name;
    }

    public StudyProgram(Long id, String name) {
        this.id = id;
        this.name = name;
    }

//    @GeneratedValue
    @Id // By default Hibernate Search uses the field marked with @Id to store the entity identifier in the index
    private Long id;

    @Field(
            index = org.hibernate.search.annotations.Index.YES,
            store = Store.NO,
            analyze = Analyze.YES
    )
    @Column(unique = true)
    private String name;

    // Collections are not cached by default.
    // Only ids of entities contained in a collection are cached for each collection entry.
//    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "student-list")
    @IndexedEmbedded(depth = 1)
    @OneToMany(mappedBy = "studyProgram")
    public List<Student> students;

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }
}
