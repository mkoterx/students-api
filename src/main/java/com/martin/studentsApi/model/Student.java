package com.martin.studentsApi.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.martin.studentsApi.json.StudentDeserializer;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.hibernate.search.annotations.*;

import javax.persistence.*;

@Indexed // Marks User as an entity which needs to be indexed by Hibernate Search
@Entity
@Table(name = "students")
@JsonDeserialize(using = StudentDeserializer.class)
/*
@Cacheable //  good convention, although not required by Hibernate
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "student") */
public class Student {

    public Student() {} // JPA Only

    public Student(Long id, String firstName, String lastName, StudyProgram studyProgram) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.studyProgram = studyProgram;
    }

    public Student(String firstName, String lastName, StudyProgram studyProgram) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.studyProgram = studyProgram;
    }

    @Id // By default Hibernate Search uses the field marked with @Id to store the entity identifier in the index
    @Column(name = "id")
//    @GeneratedValue
    private Long id;

    @Field(
            index = org.hibernate.search.annotations.Index.YES,
            store = Store.NO,
            analyze = Analyze.YES
    )
    @Column(name = "first_name")
    private String firstName;

    @Field(
            index = org.hibernate.search.annotations.Index.YES,
            store = Store.NO,
            analyze = Analyze.YES
    )
    @Column(name = "last_name")
    private String lastName;

    @IndexedEmbedded
    @ManyToOne
    @JoinColumn(name = "study_program_id")
    private StudyProgram studyProgram;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    @Override
    public String toString() {
        return String.format("[ %d, %s %s]", this.id, this.firstName, this.lastName);
    }
}
